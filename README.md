GjotsJava
=========

This is a plain Java implementation of the Gjots2 Linux software by Bob Hepple 
 [http://bhepple.freeshell.org/gjots/](http://bhepple.freeshell.org/gjots/).
 I found it difficult to get his program running on windows, so I decide to
 reimplement it. If you are on Linux use Bob's program that is more advanced and stable. 
 Anyway the file format is compatible, you can exchange the data files.
 
 Gjots is a notebook that stores text in a hierachical tree structure. 
 All data is stored to a single file that can be encrypted. 
 
![Screenshot](support/screenshot.png)
 
 Thanks to  Christoffer Soop [https://github.com/chrsoo/ccrypt-j](https://github.com/chrsoo/ccrypt-j) 
 for his ccrypt java implementation. If you like to build GjotsJava you have to 
 download his code and build his library first. 
 
### Encryption
Files may be encrypted and decrypted without external software, supported encryption formats are AES and CCRYPT. Encryption uses the same filename as the original file. You should copy or rename the file, otherwise it is overwritten when the program exits.  __Please note that GjotsJava treats a file as encrypted if a Unicode-Category 'So' character is found in the first line.__ Reading of unencrypted files fails if those characters are found in the first line. Unicode 'So' characters are listed [here](http://www.fileformat.info/info/unicode/category/So/list.htm). 

### Drag and Drop
Drag and drop support for arranging notes is still quite limited. 


### Issues and License
Please report issues and any feedback to this [issue tracker](https://bitbucket.org/herberlin/gjotsjava/issues?status=new&status=open).

License [Apache 2](http://www.apache.org/licenses/LICENSE-2.0)

### Versions
Current Stable: 
1.3.0 (2019-05-18)

Downloads [here](https://bitbucket.org/herberlin/gjotsjava/downloads/)

Download the jar - file [gjotsjava-1.3.0.jar](https://bitbucket.org/herberlin/gjotsjava/downloads/gjotsjava-1.3.0.jar) and start it per doubleclick (windows) or with `java -jar gjotsjava-1.3.0.jar`. A java - runtime version 8 or newer must be installed on your machine. 

Another way to start GjotsJava is via zero-install. Install [zero-install](http://0install.net/) for your platform, then drop the starter-file [gjots-java.xml](https://bitbucket.org/herberlin/gjotsjava/downloads/gjots-java.xml) at the zero-install starter. With zero install you benefit from automated updates. 






 
 
 


 
