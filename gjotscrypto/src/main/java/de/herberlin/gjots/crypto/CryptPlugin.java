package de.herberlin.gjots.crypto;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implementing classes provide encryption to gjots. 
 * 
 * @author (c) Hans Joachim Herbertz
 * @since 27.12.2018
 * @licence Apache 2 http://www.apache.org/licenses/LICENSE-2.0
 */
public interface CryptPlugin {
	
	InputStream getCryptedInputStream(String password, InputStream in) throws Exception;
	OutputStream getCryptedOutputStream(String passwod, OutputStream out) throws Exception;

}
