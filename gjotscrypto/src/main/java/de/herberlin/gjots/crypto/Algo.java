package de.herberlin.gjots.crypto;

public enum Algo {
	NONE, AES, CCRYPT;
}