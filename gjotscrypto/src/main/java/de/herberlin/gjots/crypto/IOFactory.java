package de.herberlin.gjots.crypto;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Read crypted files.
 * 
 * @author (c) Hans Joachim Herbertz
 * @since 30.07.2017
 * @licence Apache 2 http://www.apache.org/licenses/LICENSE-2.0
 */
public abstract class IOFactory {

	public static List<String> readStream(InputStream inputStream, Algo algo, String password) throws Exception {
		InputStream cryptedStream = getCryptedStream(inputStream, algo, password);
		BufferedReader reader = new BufferedReader(new InputStreamReader(cryptedStream));
		List<String> result = new LinkedList<String>();
		String s;
		boolean encrytionCheckDone = false;
		while ((s = reader.readLine()) != null) {
			result.add(s);
			if (!encrytionCheckDone && s != null && s.length() > 0) {
				checkIfEncrypted(s);
				encrytionCheckDone = true;
			}
		}
		return result;
	}

	private static void checkIfEncrypted(final String s) throws Exception {
		for (final char c : s.toCharArray()) {
			// unicode category 'So', this file is encrypted
			// see http://www.fileformat.info/info/unicode/category/So/list.htm
			if (Character.getType(c) == Character.OTHER_SYMBOL) {
				// System.out.println("This is encrypted to me: " + s);
				throw new Exception("Invalid line: " + s);
			}
		}
	}

	private static InputStream getCryptedStream(InputStream inputStream, Algo crypt, String password) throws Exception {
		InputStream in = null;
		switch (crypt) {
		case AES:
			Cipher cipher = Cipher.getInstance(Algo.AES.name());
			SecretKey secKey = new SecretKeySpec(hashPwd(password, Algo.AES.name()), Algo.AES.name());
			cipher.init(Cipher.DECRYPT_MODE, secKey);
			in = new CipherInputStream(inputStream, cipher);
			break;
		case CCRYPT:
			in = getCCryptPlugin().getCryptedInputStream(password, inputStream);
			break;
		case NONE:
		default:
			in = inputStream;
			break;
		}
		return in;
	}

	private static OutputStream getOutputStream(OutputStream outputStream, Algo crypt, String password)
			throws Exception {
		OutputStream out = null;

		if (password == null) {
			password = "password";
		}

		switch (crypt) {
		case AES:
			Cipher cipher = Cipher.getInstance(Algo.AES.name());
			SecretKey secKey = new SecretKeySpec(hashPwd(password, Algo.AES.name()), Algo.AES.name());
			cipher.init(Cipher.ENCRYPT_MODE, secKey);
			out = new CipherOutputStream(outputStream, cipher);
			break;
		case CCRYPT:
			out = getCCryptPlugin().getCryptedOutputStream(password, outputStream);
			break;
		case NONE:
		default:
			out = outputStream;
			break;
		}
		return out;
	}

	public static OutputStream writeToStream(List<String> lines, OutputStream out, Algo algo, String password)
			throws Exception {
		OutputStream outputStream = getOutputStream(out, algo, password);
		OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
		
		boolean first = true;
		for (String s : lines) {
			if (first) {
				first = false;
			} else {
				writer.write("\n");
			}
			writer.write(s);

		}
		writer.flush();
		writer.close();
		return outputStream;
	}

	/** MD5-Hash */
	private static byte[] hashPwd(String password, String algorithm)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(password.getBytes("UTF-8"));
		return md.digest();

	}

	private static CryptPlugin ccryptPlugin = null;

	private static CryptPlugin getCCryptPlugin()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (ccryptPlugin == null) {
			ccryptPlugin = (CryptPlugin) Class.forName("de.herberlin.gjots.CCryptPlugin").newInstance();
		}

		return ccryptPlugin;

	}

}
