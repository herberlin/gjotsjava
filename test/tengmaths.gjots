Reformatted for gjots from engmath.s5 from http://3lib.ukonline.co.uk/pocketinfo/index.html

by

Bob Hepple, bhepple @freeshell.org, Nov 2002
\NewEntry
An Engineering Maths Database for the Psion Series 5
By Kevin Millican

... and edited by Bob Hepple (removing engineering stuff, adding more maths & astronomy)

If there are any errors or if you would like to submit additional records for inclusion, please contact me at :-
Email: kevin.millican@altavista.net
\NewEntry
astro
\NewFolder
\NewEntry
Earth

Polar radius 		6356.8km
Equatorial radius 	6378.2km
Mean radius	 	6371km
Surface area	 	5.101*10**14 m�
Volume 			1.083*10**21 m�
Mass	 			5.977*10**27 g
Mean distance to Sun (1AU)
					1.496*10**11 m
Escape velocity at surface
					11.2 km/s
Rotational velocity at equator
					465 m/s
Mean velocity about sun
					29.78 km/s
\NewEntry
Moon

radius				1738km
Surface area		3.796*10**m�
Sideral period of moon about earth
					27.32 solar days
Mean synodical or lunar month
					29.531 mean solar days
Mean distance from the earth
					3.844*10**8 m
Escape velocity at surface
					2.38 km/s
Angle subtended in the sky
					� degree approx
\NewEntry
Sun

Radius				6.960*10**8 m
Surface area		6.087*10**m�
Mass				1.99*10**30 kg
\NewEntry
Time

Mean solar day	 86400 mean solar seconds
Sidereal day		 86164.090 mean solar seconds
tropical (civil) year 365.242 mean solar days
					 31556925.9747 s
sidereal year		 365.256 mean solar days
\NewEntry
Distance

1AU		1.495985*10**11 m
1 Parsec	3.0856*10**16 m
1 Parsec	2.062648*10**5 AU
1 Parsec	3.2615 ly
ly			9.45605*10**15 m
			6.324*10**4 AU
			0.3066 pc
\EndFolder
\NewEntry
General
\NewFolder
\NewEntry
Decimal Prefixes

T	tera	10**12
G	giga	10**9
M	mega	10**6
k	kilo 	10**3
h	hecto	10**2
da	deca	10
d	deci	10**-1
c	centi	10**-2
m	milli 	10**-3
�	micro	10**-6
n	nano	10**-9
p	pico	10**-12
\EndFolder
\NewEntry
Maths
\NewFolder
\NewEntry
Circle Equation

x�+y�=a�
A = pi*r� = (pi/4)D�
\NewEntry
Ellipse Equation

x�/a� + y�/b� = 1
A =  pi*r1*r2 =  pi*D1*D2 / 4
\NewEntry
Hyperbola Equation

x�/a� - y�/b� = 1
\NewEntry
Parabola Equation

y� = ax
\NewEntry
Triangle Area

A = b H/2
A = �bc sin(A)
 
where b is the base length and H is the height
\NewEntry
Sphere

A = 4 * pi * r�
V = 4/3 * pi * r�
area cut off on sphere by parallel planes h apart = 2 * pi * rh
\NewEntry
Cone

V = pi * r�h/3
A = pi * r * l 

where l=slant length A=area of curved surface
\NewEntry
Cylinder

A = 2 * pi * r(h+r)
V = pi * r�h
\NewEntry
Series ex

ex = 1 + x + x�/2! + ... + x**n/n!
\NewEntry
Cosine Series

cos(x) = 1-x�/2! + x**4/4! - ... (-1)**n x**2n / (2n)! + ...
\NewEntry
Sine Series

sin(x) = x - x�/3! + x**5/5! - ... + (-1)**n x**(2n+1) / (2n+1)! + ...
\NewEntry
Tangent Series

tan(x) = x + x�/3 + 2x**5/15 + 17x**7/315 + ...
 
for x<pi/2
\NewEntry
Logs

ln(1+x) = x - x�/2 + x�/3 - ... + (-1)**n x**(n+1) / (n+1)
 
for -1 < x <= 1

log-a(x) = y iff x=a**y
log-q(p)p = log-q(r) * log-r(p)

where log-q(x) is the log to base q of x
\NewEntry
Differential/Integral xn

d/dx x**n = n x**n-1

integral(x**n dx) = x**n+1 / (n+1)
\NewEntry
Differential ln(x)/Integral 1/x

d/dx ln(x) = 1/x
 
integral(1/x dx) = ln(x)
\NewEntry
Differential/Integral eax

d/dx e**ax = a e**ax
 
 integral(eax dx) = (e**x)/a
\NewEntry
Differential/Integral ax

d/dx a**x = a**x ln(a)
 
integral(a**x dx) = a**x / ln(a)
\NewEntry
Differential/Integral xx

d/dx x**x = x**x (1+ln(x))
 
 integral(ln(x) dx) = x(ln(x)-1)
\NewEntry
Differential sin(x)

d/dx sin(x) = cos(x)
 
integral(cos(x) dx) = sin(x)
\NewEntry
Differential cos(x)

d/dx cos(x) = -sin(x)
 
integral(sin(x) dx) = -cos(x)
\NewEntry
Differential tan(x)

d/dx tan(x) = sec�(x)
 
integral(sec�(x) dx) = tan(x)
\NewEntry
Differential cot(x)

d/dx cot(x) = -cosec�(x)
 
integral(cosec�(x) dx) = -cot(x)
\NewEntry
Differential sin-1(x)

d/dx sin-1(x) = 1/(1-x�)
 
integral(1/sqrt(1-x�) dx) = sin**-1(x), |x| < 1
\NewEntry
Differential tan-1(x)

d/dx tan**-1(x) = 1/(1+x�)
 
integral(1/(1+x�) dx) = tan**-1(x)
\NewEntry
Differential cosh(x)

d/dx cosh(x) = sinh(x)
 
integral(sinh(x) dx) = cosh(x)
\NewEntry
Differential sinh(x)

d/dx sinh(x) = cosh(x)
 
integral(cosh(x) dx) = sinh(x)
\NewEntry
Differential tanh(x)

d/dx tanh(x) = sech2(x)
 
integral(sech�(x) dx) = tanh(x)
\NewEntry
Differential coth(x)

d/dx coth(x) = -cosech2(x)
 
integral(cosech�(x) dx) = -coth(x)
\NewEntry
Differential sinh-1(x)

d/dx sinh-1(x) = 1/sqrt(1+x�)
 
integral(1/sqrt(1+x�) dx) = sinh**-1(x)

= ln(x + sqrt(x�+1))
\NewEntry
Differential cosh-1(x)

d/dx cosh**-1(x) = 1/sqrt(x�-1)
 
integral(1/sqrt(x�-1) dx) = cosh**-1(x)

= ln(x + sqrt(x�-1)), where x>=1
\NewEntry
Differential tanh-1(x)

d/dx tanh**-1(x) = 1/(1-x�)
 
integral(1/(1-x�) dx) = tanh**-1(x)

= �ln((1+x)/(1-x)), where x�<1
\NewEntry
Differential coth-1(x)

d/dx coth**-1(x) = 1/(1-x�)
 
integral(1/(x�-1) dx) = -coth**-1(x)

= �ln((x-1)/(x+1)), where x�>1
\NewEntry
Rules of Differentiation

Product Rule:
d/dx (uv) = u dv/dx + v du/dx
 
d/dx (uvw) = uv dw/dx + uw dv/dx + vw du/dx

Quotient Rule:
d/dx (u/v) = 1/v� (v du/dx - u dv/dx)

Chain Rule (function of a function):
dy/dt = dy/dx dx/dt
(where y is a function of x and x is a function of t)
\NewEntry
Rules of Integration

integral(uv dx) = uw - integral(du/dx w dx), 

where w = integral(v dx)
\NewEntry
Trigonometric Rules 1

2sin(A)cos(B) = sin(A-B)+sin(A+B)
2cos(A)cos(B) = cos(A-B)+cos(A+B)
2sin(A)sin(B) = cos(A-B)-cos(A+B)

sin(A)+sin(B) = 2sin�(A+B)cos�(A-B)
sin(A)-sin(B) = 2cos�(A+B)sin�(A-B)
cos(A)+cos(B) = 2cos�(A+B)cos�(A-B)
cos(A)-cos(B) = -2sin�(A+B)sin�(A-B)

sin(A�B)=sin(A)cos(B)+/-cos(A)sin(B)
cos(A�B)=cos(A)cos(B)-/+sin(A)sin(B)
tan(A�B)=(tan(A)+/-tan(B))/(1-/+tan(A)tan(B))

sin�(A)+cos�(A) = 1
sec�(A) = tan�(A)+1
\NewEntry
Trigonometric Rules 2

In triangle ABC with sides abc and angle ABC (side a is opposite
angle A)

a/sin(A) = b/sin(B) = c/sin(C) (sine rule)

sin(A)=2/bc sqrt(s(s-a)(s-b)(s-c)),
where s=(a+b+c)/2 = (2/bc)area

a� = b� + c� - 2bc cos(A) (cosine rule)
\NewEntry
Roots of a Quadratic Equation

(-b � sqrt(b2-4ac))/2a
\NewEntry
Quadratic Expansion

a�-b� = (a+b)(a-b)

a�-b� = (a-b)(a�+ab+b�)
\NewEntry
Sums

sum(r) = �n(n+1) : r=1..n
r� = n(n+1)(2n+1)/6 : r=1..n
r� = �n�(n+1)� : r=1..n
a**r = (1-a**n)/(1-a) : r=0..n-1

...where sum(r) is the sum of all terms r - normally represented
by a Greek capital sigma.
\NewEntry
Taylor's expansion

f(a+x)=f(a) + xf'(a) + x�/2!f''(a) + x�/3!f'''(a) + ...
\NewEntry
Binomial

(1+x)**n = 1 + nx + n(n-1)x�/2! + n(n-1)(n-2)x�/3! + ...

where |x| < 1
\NewEntry
Simpson's Rule

integral(f(x) dx) ~= h(y0 + 4y1 + y2)/3 where

a and b are the limits of the integral
h = (b - a)/2
y0 = f(a)
y1 = f((a+b)/2)
y2 = f(b)
\EndFolder
\NewEntry
Physics
\NewFolder
\NewEntry
Loudness

0	threshold of hearing
10	virtual silence
20	quiet room
30	watch ticking at 1m
40	quiet street
50	quiet conversation
60	quiet motor at 1m
70	loud conversation
80	door slamming
90	busy typing room
100	near loud motor horn
110 	pneumatic drill
120 	near aero engine
130 	threshold of pain
\NewEntry
Refractive index

Air 1.000292
Water 1.333
Glass 1.48-1.61
Diamond 2.417
\NewEntry
Temperatures

All in  �C
Absolute zero 	-273.15
BP Helium 	-268.93 (4.216�K)
FP hydrogen 	-259.2 (14�K)
BP hydrogen 	-252.8 (20.4�K)
FP nitrogen 	-209.9 (63.3�K)
BP nitrogen 	-195.8 (77.32�K)
FP oxygen 	-218.4 (54.8�K)
BP oxygen 	-182.97
FP mercury 	-38.87
FP water 	0
BP water 	100
FP lead 	327.3
BP mercury 	356.58
BP sulphur 	444.6
Dull red heat 	500-600
FP aluminium 	660.1
FP silver 	960.8
FP gold 	1063
White heat 	1500-1800
\NewEntry
Hardness (Moh's scale)

 1 Talc
 2 rock salt
(2.5 finger nail)
 3 calcite
 4 fluorite
 5 apatite
 6 felspar
(6 penknife)
 7 quartz
 8 topaz
 9 corundum (emery)
10 diamond
\NewFolder
\NewEntry
Sound velocity

Air (0�C)	331.3 m/s
Hydrogen (0�C)	1284 m/s
Oxygen (0�C)	316 m/s
Water (25�C)	1498 m/s
\EndFolder
\NewEntry
Newtons Laws

1. An object will remain at rest or in uniform motion in a straight line unless acted on by an external, unbalanced force.

2. The acceleration of an object is directly proportional to the net force acting on it and is inversely proportional to its mass. The direction of the acceleration is in the direction of the applied net force.

(ie. F=ma)

3. For every action there is an equal and opposite reaction.
\NewEntry
Basic Motion Laws

u = initial velocity
v = final velocity
s = distance
a = acceleration
t = time

s = �(u+v)t
v = u+at
s = ut+�at�
v� = u�+2as
\EndFolder
