package de.herberlin.gjots.parser;

import java.util.LinkedList;
import java.util.List;

/**
 * Node.
 *
 * @author (c) Hans Joachim Herbertz
 * @since 30.07.2017
 * @licence Apache 2 http://www.apache.org/licenses/LICENSE-2.0
 */
public class NodeImpl implements Node {

	private String title;
	private String text;
	private Node parent;
	private List<Node> children = new LinkedList<Node>();
	private final StringBuilder b = new StringBuilder();
	private final String nl = "\n";
	private GjotsParser model = null;

	public NodeImpl(final GjotsParser model) {
		this.model = model;
	}

	public NodeImpl(final GjotsParser model, final Node parent) {
		this.model = model;
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#getTitle()
	 */
	@Override
	public String getTitle() {
		return this.title;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#setText(java.lang.String)
	 */
	@Override
	public void setText(final String s) {
		this.text = s;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#getText()
	 */
	@Override
	public String getText() {
		if (this.text == null) {
			this.text = this.b.toString();
		}
		return this.text;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#setTitleAndText(java.lang.String)
	 */
	@Override
	public void setTitleAndText(final String title) {
		this.title = title;
		this.b.append(title);
		this.b.append(this.nl);
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(final String title) {
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#getParent()
	 */
	@Override
	public Node getParentNode() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#setParent(de.herberlin.gjots.parser.NodeImpl)
	 */
	@Override
	public void setParent(final Node parent) {
		this.parent = (Node) parent;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#getChildren()
	 */
	@Override
	public List<Node> getChildNodes() {
		return this.children;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#hasChildren()
	 */
	@Override
	public boolean hasChildren() {
		return !this.children.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#setChildren(java.util.List)
	 */
	@Override
	public void setChildren(final List<Node> children) {
		this.children = children;
	}

	@Override
	public void appendLine(final String line) {
		this.b.append(line);
		this.b.append(this.nl);
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#addChild(de.herberlin.gjots.parser.NodeImpl)
	 */
	@Override
	public Node addChild(final Node node) {
		this.children.add(node);
		return node;

	}

	@Override
	public String toString() {
		return this.title;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#contains(java.lang.String)
	 */
	@Override
	public boolean contains(final String s) {
		return getText().toLowerCase().contains(s);
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#startPositions(java.lang.String)
	 */
	@Override
	public List<Integer> startPositions(String s) {
		String rr = getText().toLowerCase();
		List<Integer> result = new LinkedList<Integer>();
		int x = 0;
		do {
			x = rr.indexOf(s, x);
			if (x < 0) {
				break;
			}
			result.add(x);
			x++;
		} while (true);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see de.herberlin.gjots.parser.Node#updateText(java.lang.String)
	 */
	@Override
	public void updateText(String text) {
		this.setText(text);
		if (this.text.indexOf('\n') > 0) {
			this.setTitleAndText(this.text.substring(0, this.text.indexOf('\n')));
		}
		this.model.setUnsavedChanges(true);
	}

}
