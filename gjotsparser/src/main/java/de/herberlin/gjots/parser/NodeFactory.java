package de.herberlin.gjots.parser;

/**
 * NodeFactory provides Node Implementation.
 *
 * @author aherbertz
 * @created 2018-01-17
 *
 */
public abstract class NodeFactory {

	public static Node getNode(GjotsParser parser, Node parent) {
		return _instance.createNode(parser, parent);
	}

	public abstract Node createNode(GjotsParser parser, Node parent);

	public static void setInstance(NodeFactory factory) {
		_instance = factory;
	}

	private static NodeFactory _instance = new NodeFactory() {
		@Override
		public Node createNode(GjotsParser parser, Node parent) {
			return new NodeImpl(parser, (Node) parent);
		}
	};

}
