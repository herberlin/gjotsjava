package de.herberlin.gjots.parser;

import java.util.List;

public interface Node {

	void appendLine(String line);

	String getTitle();

	void setText(String s);

	String getText();

	void setTitleAndText(String title);

	void setTitle(String title);

	Node getParentNode();

	void setParent(Node parent);

	List<Node> getChildNodes();

	boolean hasChildren();

	void setChildren(List<Node> children);

	Node addChild(Node node);

	boolean contains(String s);

	List<Integer> startPositions(String s);

	void updateText(String text);

}
