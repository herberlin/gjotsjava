package de.herberlin.gjots.parser;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import de.herberlin.gjots.parser.Node;

/**
 * Tree model. Reads, saves and parses files.
 *
 * @author (c) Hans Joachim Herbertz
 * @since 30.07.2017
 * @licence Apache 2 http://www.apache.org/licenses/LICENSE-2.0
 */
public class GjotsParser {

	private static String CMD_NEW_ENTRY = "\\NewEntry";
	private static String CMD_NEW_FOLDER = "\\NewFolder";
	private static String CMD_END_FOLDER = "\\EndFolder";
	private static String NEW_ITEM_TEXT = "New Item";

	private boolean unsavedChanges = false;

	private Node root;
	private final Set<Node> all = new LinkedHashSet<Node>();
	private boolean rootVisible = true;

	public void readGjots(List<String> source, String title) {

		this.root = NodeFactory.getNode(this, null);
		root.setTitle(title);
		Node parent = this.root;
		Node currentNode = null;
		for (String s : source) {
			if (CMD_NEW_ENTRY.equals(s)) {
				currentNode = NodeFactory.getNode(this, parent);
				parent.addChild(currentNode);
				this.all.add(currentNode);
			} else if (CMD_NEW_FOLDER.equals(s)) {
				parent = currentNode;
			} else if (CMD_END_FOLDER.equals(s)) {
				parent = parent.getParentNode();
			} else if (currentNode == null) {
				// first node starts without command
				// means single root entry
				currentNode = this.root;
				currentNode.setTitle(root.getTitle());
				currentNode.appendLine(s);
				this.rootVisible = true;
				this.all.add(currentNode);
			} else if (currentNode.getTitle() == null) {
				currentNode.setTitleAndText(s);
			} else {
				currentNode.appendLine(s);
			}
		}
	}

	public List<String> writeGjots(){
		List<String> result = new LinkedList<>();
		if (this.rootVisible && this.root.getText() != null) {
			String s = root.getText();
			result.add(s);
		}
		for (Node node : this.root.getChildNodes()) {
			this.writeNode(node, result);
		}
		return result;
	}

	public boolean isUnsavedChanges() {
		return this.unsavedChanges;
	}

	public void setUnsavedChanges(final boolean val) {
		this.unsavedChanges = val;
	}

	private void writeNode(final Node node, List<String> list){

		list.add(CMD_NEW_ENTRY);
		list.add(node.getText());

		if (node.hasChildren()) {
			list.add(CMD_NEW_FOLDER);
			for (final Node child : node.getChildNodes()) {
				this.writeNode(child, list);
			}
			list.add(CMD_END_FOLDER);
		}
	}

	public void newItemAfter(final Node node) {
		final Node newNode = NodeFactory.getNode(this, node.getParentNode());
		newNode.setTitleAndText(NEW_ITEM_TEXT);
		this.appendAfter(node, newNode);
		this.all.add(newNode);
	}

	public int getChildPosition(final Node parent, final Node child) {
		int result = 0;
		if (parent != null && child != null) {
			result = parent.getChildNodes().indexOf(child);
		}
		return result;
	}

	private void appendAfter(final Node node, final Node newNode) {
		final int pos = getChildPosition(node.getParentNode(), node);
		if (pos < 0) {
			node.getParentNode().addChild(newNode);
		} else {
			node.getParentNode().getChildNodes().add(pos + 1, newNode);
		}
		newNode.setParent(node.getParentNode());
		this.unsavedChanges = true;
	}

	public void appendToRoot() {
		final Node newNode = NodeFactory.getNode(this, this.root);
		newNode.setTitle("New Item");
		this.all.add(newNode);
		this.appendChild(this.root, newNode);
	}

	public void newChild(final Node node) {
		final Node newNode = NodeFactory.getNode(this, node.getParentNode());
		newNode.setTitleAndText(NEW_ITEM_TEXT);
		this.all.add(newNode);
		this.appendChild(node, newNode);
	}

	private void appendChild(final Node node, final Node child) {
		node.getChildNodes().add(0, child);
		child.setParent(node);
		this.unsavedChanges = true;
	}

	public void deleteNode(final Node node) {
		if (!this.root.equals(node)) {
			node.getParentNode().getChildNodes().remove(node);
			this.all.remove(node);
			this.unsavedChanges = true;
		}
	}

	public void moveNode(final Node node, final Node parent, final Node nullIfChild) {
		if (node != parent && node != parent.getParentNode()) {
			this.deleteNode(node);
			if (nullIfChild == null) {
				this.appendChild(parent, node);
			} else {
				this.appendAfter(parent, node);
			}
		}
	}

	public boolean isRootVisible() {
		return this.rootVisible;
	}

	public Node getRoot() {
		return root;
	}

	public Collection<Node> getAllNodes() {
		return this.all;
	}
}
