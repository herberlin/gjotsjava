#!/bin/bash

# Creates the files to updload for 0install
# Version must not contain letterns, only numbers

set -e

VERSION=$1
echo Creating version $VERSION

# make zipfile and jars
rm -r build 
mkdir build
cp ../gjotsdesktop/target/gjotsdesktop-$VERSION.jar build/
cd build
cp gjotsdesktop-$VERSION.jar gjotsjava.jar
chmod +x gjotsdesktop-$VERSION.jar
zip gjots-java-$VERSION.zip gjotsjava.jar
rm gjotsjava.jar
mv gjotsdesktop-$VERSION.jar gjotsjava-$VERSION.jar


# manifest
cp ../gjots-java.xml .
sed -i "s/VERSION/$VERSION/g" gjots-java.xml
0publish gjots-java.xml \
	--set-version=$VERSION --set-released=today \
	--archive-url=https://bitbucket.org/herberlin/gjotsjava/downloads/gjots-java-$VERSION.zip
0publish gjots-java.xml --xmlsign
