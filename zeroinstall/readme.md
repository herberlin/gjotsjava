## Zero Install 

### Docs
- Install http://0install.net/install-linux.html
- Tutorial http://0install.net/dev.html
- Beispiel MediathekView http://0install.de/feeds/Mediathek.xml

### Install 0publish
0install add 0publish http://0install.net/2006/interfaces/0publish

### insert version
implementation tag required:

 &lt;implementation id="." released="" stability="stable" version="VERSION">&lt;/implementation>

0publish gjots-java.xml --set-version=1.3.0 --set-released=today --archive-url=https://bitbucket.org/herberlin/gjotsjava/downloads/gjots-java-1.3.0.zip

### sign file
[gpg --gen-key] (schon geschehen)

0publish gjots-java.xml --xmlsign
