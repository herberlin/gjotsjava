package de.herberlin.gjots;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKey;

import org.bouncycastle.crypto.engines.RijndaelEngine;

import de.herberlin.gjots.crypto.CryptPlugin;
import se.jabberwocky.ccrypt.CCryptInputStream;
import se.jabberwocky.ccrypt.CCryptOutputStream;
import se.jabberwocky.ccrypt.jce.CCryptKeySpec;
import se.jabberwocky.ccrypt.jce.CCryptSecretKeyFactorySpi;

/**
* @author (c) Hans Joachim Herbertz
* @since 27.12.2018
* @licence Apache 2 http://www.apache.org/licenses/LICENSE-2.0
*/
public class CCryptPlugin implements CryptPlugin{

	@Override
	public InputStream getCryptedInputStream(String password, InputStream in) throws InvalidKeySpecException, IOException {
		return new CCryptInputStream(generateSecret(password), in);
	}

	@Override
	public OutputStream getCryptedOutputStream(String password, OutputStream out) throws InvalidKeySpecException, IOException {
		return new CCryptOutputStream(generateSecret(password), out);
	}

	/**
	 * CCrypt hash https://github.com/chrsoo/ccrypt-j
	 * 
	 * @param password
	 * @return
	 * @throws InvalidKeySpecException
	 */
	private static SecretKey generateSecret(String password) throws InvalidKeySpecException {
		CCryptSecretKeyFactorySpi keyFactory = new CCryptSecretKeyFactorySpi(new RijndaelEngine(256));
		return keyFactory.engineGenerateSecret(new CCryptKeySpec(password));
	}
	
}
