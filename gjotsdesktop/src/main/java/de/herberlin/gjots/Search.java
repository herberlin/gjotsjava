package de.herberlin.gjots;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import de.herberlin.gjots.parser.Node;

public class Search extends JPanel {

	private Main main = null;
	private final JTextField textField = new JTextField(20);
	private Action searchNextAction = null;
	private Action searchPrevAction = null;
	JButton buttonR, buttonL;

	public Search(final Main main) {
		this.main = main;
		init();
	}

	private void init() {
		setLayout(new FlowLayout(FlowLayout.LEFT));
		add(buttonL = new JButton(searchPrevAction = new AppAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				if (searchResult == null) {
					search();
				}
				if (searchResult.size() > 0) {
					currentSearchResult--;
					if (currentSearchResult < 0) {
						currentSearchResult = searchResult.size() - 1;
					}
					UIUtils.updateTree(main.getTree(),
							searchResult.get(currentSearchResult));
				}
			}

			@Override
			void init() {
				putValue(Action.NAME, "<");
				putValue(Action.ACCELERATOR_KEY,
						KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.ALT_DOWN_MASK));
			}
		}));
		add(textField);

		searchNextAction = new AppAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				final boolean resultIsNew = search();
				if (searchResult.size() > 0) {
					if (!resultIsNew) {
						currentSearchResult++;
					}
					if (currentSearchResult >= searchResult.size()) {
						currentSearchResult = 0;
					}
					UIUtils.updateTree(main.getTree(),
							searchResult.get(currentSearchResult));
				}
			}

			@Override
			void init() {
				putValue(Action.NAME, ">");
				putValue(Action.ACCELERATOR_KEY,
						KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.ALT_DOWN_MASK));
			}
		};
		add(buttonR = new JButton(searchNextAction));
		setVisible(false);

		textField.addActionListener(searchNextAction);
	}

	public JMenuItem getSearchMenu() {

		Action openSearchAction = null;
		JMenu menu = new JMenu("View");
		menu.add(new JMenuItem(openSearchAction = new AppAction() {
			@Override
			void init() {
				putValue(Action.NAME, "Search");
				// putValue(Action.SMALL_ICON,
				// UIUtils.getIcon("/images/ic_open_in_browser_black_24dp_1x.png"));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK));
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);

			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				setVisible(!isVisible());
				searchResult = null; 
				lastKeyword = null; 
				textField.setText("");
				if (isVisible()) {
					textField.grabFocus();
				} else {
					searchResult = null; 
				}
				
			}
		}));
		menu.setAction(openSearchAction);
		menu.add(new JMenuItem(searchPrevAction));
		menu.add(new JMenuItem(searchNextAction));
		return menu;
	}

	private int currentSearchResult = 0;
	private List<Node> searchResult = null;
	private String lastKeyword;

	public String getSearch() {
		String result = null; 
		if (isVisible() && searchResult != null && !searchResult.isEmpty()){
			result = lastKeyword;
		}
		return result; 
	}
	private boolean search() {
		String keyword = textField.getText();
		boolean result = false;
		if (keyword != null) {
			keyword = keyword.toLowerCase();
			if (!keyword.equals(lastKeyword)) {
				final Collection<Node> all = main.getModel().getAllNodes();
				searchResult = new ArrayList<Node>();
				currentSearchResult = 0;
				lastKeyword = keyword;
				if (keyword.length() > 0) {
					result = true;
					for (final Node n : all) {
						if (n.contains(keyword)) {
							searchResult.add(n);
						}
					}
				}
				final boolean hasResults = !searchResult.isEmpty();
				buttonR.setEnabled(hasResults);
				buttonL.setEnabled(hasResults);
			}
		}
		return result;
	}
}
