package de.herberlin.gjots;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import de.herberlin.gjots.crypto.Algo;
import de.herberlin.gjots.crypto.IOFactory;
import de.herberlin.gjots.parser.GjotsParser;
import de.herberlin.gjots.parser.Node;

public class GjotsTreeModel extends GjotsParser implements TreeModel {

	private File currentFile = null;
	private String password = "password";
	private Algo currentAlgo = Algo.NONE;

	public GjotsTreeModel(File file) throws Exception {
		try {
			this.currentFile = file;
			this.currentAlgo = Algo.NONE;
			readFile(new FileInputStream(file),file.getName(), Algo.NONE, null);
			return;
		} catch (final Exception e) {
			System.err.println("Error reading " + file);
		}

		throw new Exception("Unknown file format: " + file);
	}

	private void readFile(FileInputStream fileInputStream,String filename,  Algo algo, String password) throws Exception {
		List<String> list = IOFactory.readStream(fileInputStream, algo, password ); 
		readGjots(list, filename);
	}

	public GjotsTreeModel(final File file, final String password) throws Exception {
		this.password = password;
		for (final Algo a : Algo.values()) {
			try {
				this.currentFile = file;
				this.currentAlgo = a;

				this.readFile(new FileInputStream(file), file.getName(), a, password);
				return;
			} catch (final Exception e) {
				System.err.println("Error reading " + a);
				e.printStackTrace();
			}
		}

		throw new Exception("Unknown file format: " + file);
	}

	@Override
	public void addTreeModelListener(final TreeModelListener l) {
	}

	@Override
	public void removeTreeModelListener(final TreeModelListener l) {
	}


	@Override
	public Object getChild(final Object parent, final int index) {
		Node result = null;
		if (parent != null) {
			result = (Node) ((Node) parent).getChildNodes().get(index);
		}
		return result;
	}

	@Override
	public int getChildCount(final Object p) {
		int result = 0;
		if (p != null) {
			result = ((Node) p).getChildNodes().size();
		}
		return result;
	}

	@Override
	public boolean isLeaf(final Object n) {
		return ((Node) n).getChildNodes().isEmpty();
	}

	@Override
	public void valueForPathChanged(final TreePath path, final Object newValue) {
	}

	@Override
	public int getIndexOfChild(final Object parent, final Object child) {

		return getChildPosition((Node) parent, (Node) child);
	}

	public File getCurrentFile() {
		return this.currentFile;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public Algo getCurrentAlgo() {
		return this.currentAlgo;
	}

	public void save(final File file, Algo algo, String password) throws Exception {
		currentAlgo = algo;
		this.password = password; 
		this.currentFile = file; 
		OutputStream out = IOFactory.writeToStream(writeGjots(), new FileOutputStream(file), algo, password);
		out.flush();
		out.close();

		setUnsavedChanges(false);
	}
}
