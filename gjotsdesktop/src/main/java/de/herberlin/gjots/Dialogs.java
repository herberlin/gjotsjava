package de.herberlin.gjots;

import java.awt.Container;
import java.awt.Dialog.ModalityType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import de.herberlin.gjots.crypto.Algo;

public class Dialogs {

	private static final int padding = 5;

	public static void saveAsDialog(Main main) {

		GjotsTreeModel model = main.getModel();
		JDialog d = new JDialog(main.getOwner(), "Save as");
		d.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		d.setModal(true);
		Container c = d.getContentPane();
		JPanel p = new JPanel();
		p.setBorder(new EmptyBorder(padding, padding, padding, padding));
		c.add(p);
		p.setLayout(new GridLayout(5, 2, 10, 5));
		p.add(new JLabel("Save file: " + model.getCurrentFile().getName()));
		p.add(new JLabel());
		p.add(new JLabel("Encryption:"));

		JComboBox<Algo> box = new JComboBox<Algo>(Algo.values());
		box.setSelectedItem(model.getCurrentAlgo());
		p.add(new FlowPanel(box));

		p.add(new JLabel("New File:"));
		JButton chooseButton = new JButton("Choose:");
		JButton ok = new JButton("Save");
		p.add(new FlowPanel(chooseButton));

		JLabel newFilePath = new JLabel();
		p.add(newFilePath);
		JLabel newFileName = new JLabel();
		p.add(newFileName);
		JFileChooser chooser = new JFileChooser(model.getCurrentFile());
		chooseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = chooser.showSaveDialog(d);
				if (result == JFileChooser.APPROVE_OPTION) {
					newFilePath.setText(chooser.getSelectedFile().getPath());
					newFileName.setText(chooser.getSelectedFile().getName());
					ok.setEnabled(true);

				}
			}
		});

		ok.setEnabled(false);
		ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				// save current file
				main.saveFile(null, null);

				Algo algo = (Algo) box.getSelectedItem();
				File file = chooser.getSelectedFile();
				if (algo != Algo.NONE){
					changePasswordDialog(main, file, algo);
				} else {
					main.saveFile(file, algo);
				}
				d.dispose();

			}
		});

		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				d.dispose();
			}
		});

		p.add(new FlowPanel(cancel));
		p.add(new FlowPanel(ok));

		d.pack();
		d.setVisible(true);

	}

	private static class FlowPanel extends JPanel {
		public FlowPanel(JComponent c) {
			super(new FlowLayout(FlowLayout.LEFT));
			add(c);
		}
	}

	public static void changePasswordDialog(Main main, File file, Algo algo) {

		JDialog d = new JDialog(main.getOwner(), "Set Password", ModalityType.APPLICATION_MODAL);
		// d.setModal(true);
		Container c = d.getContentPane();
		JPanel p = new JPanel();
		p.setBorder(new EmptyBorder(padding, padding, padding, padding));
		c.add(p);

		p.setLayout(new GridLayout(5, 1, 10, 5));
		p.add(new JLabel("Please enter your new password."));

		JPasswordField p1 = new JPasswordField();
		p.add(p1);
		p.add(new JLabel("Please repeat your password."));
		JPasswordField p2 = new JPasswordField();
		p.add(p2);

		JButton ok = new JButton("Ok");
		JButton cancel = new JButton("Cancel");
		JPanel p3 = new JPanel(new FlowLayout());
		p3.add(ok);
		p3.add(cancel);
		p.add(p3);
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				d.dispose();
			}
		});
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GjotsTreeModel model = main.getModel();
				String t = String.valueOf(p1.getPassword());
				if (t == null || t.length() < 3) {
					JOptionPane.showMessageDialog(d, "Password to short.");
				} else if (t.equals(String.valueOf(p2.getPassword()))) {
					if (model != null) {
						model.setPassword(t);
						main.saveFile(file, algo);
					}
					d.dispose();
				} else {
					JOptionPane.showMessageDialog(d, "Passwords not matching.");
				}
			}
		});
		d.pack();
		d.setVisible(true);

	}
}
