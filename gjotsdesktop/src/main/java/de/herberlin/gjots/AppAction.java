package de.herberlin.gjots;

import javax.swing.AbstractAction;

public abstract class AppAction extends AbstractAction {
	private static final long serialVersionUID = 1L;

	public AppAction() {
		this.init();
	}

	abstract void init();
}