package de.herberlin.gjots;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import de.herberlin.gjots.parser.Node;

public class DocumentListenerProxy implements DocumentListener {

	private Node node = null;

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	private void updateNode(DocumentEvent e) {
		if (node != null) {

			Document doc = e.getDocument();
			try {
				String text = doc.getText(0, doc.getLength());
				node.updateText(text);
				UIUtils.toaster.setStatus(null);
			} catch (BadLocationException e1) {
				System.err.println(e);
			}
		}
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		updateNode(e);
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		updateNode(e);
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		updateNode(e);
	}

}
