package de.herberlin.gjots;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import de.herberlin.gjots.parser.Node;

/**
 * UI Utils.
 *
 * @author (c) Hans Joachim Herbertz
 * @since 30.07.2017
 * @licence Apache 2 http://www.apache.org/licenses/LICENSE-2.0
 */
public abstract class UIUtils {

	private UIUtils() {
	}

	public static Toaster toaster = null;

	public static interface Toaster {
		void toast(String text);

		void setStatus(String text);
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
	public ImageIcon createImageIcon(final String path, final String description) {
		final java.net.URL imgURL = this.getClass().getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	public static Node getNodeFromTreePath(final TreePath tp) {
		Node result = null;
		if (tp != null) {
			final Object o = tp.getLastPathComponent();
			if (o != null && o instanceof Node) {
				result = (Node) o;
			}
		}
		return result;
	}

	public static void updateTree(JTree tree, Node node) {
		List<Node> list = new LinkedList<Node>();
		Node n = node;
		do {
			list.add(n);
			n = n.getParentNode();
		} while (n != null);
		Node[] arr = new Node[list.size()];
		int i = 0;
		for (Node nn : list) {
			i++;
			arr[list.size() - i] = nn;
		}

		TreePath tp = new TreePath(arr);
		updateTree(tree, tp);

	}

	public static void updateTree(final JTree tree, final TreePath path) {
		if (tree != null) {
			final TreeModel model = tree.getModel();
			tree.setModel(null);
			tree.setModel(model);
			if (path != null) {
				tree.setSelectionPath(path);
				tree.expandPath(path);
				tree.scrollPathToVisible(path);
			}
		}
	}

	public static ImageIcon getIcon(final String path) {
		try {
			final URL imgURL = UIUtils.class.getResource(path);
			return new ImageIcon(imgURL);
		} catch (final Exception e) {
			throw new RuntimeException("Resource not found: " + path, e);
		}
	}

	public static String ucFirst(final String text) {
		String r = text;
		if (text != null) {
			r = text.replaceAll("-", " ");
			final char[] c = r.toCharArray();
			c[0] = Character.toUpperCase(c[0]);
			r = String.valueOf(c);
		}
		return r;
	}

}
