package de.herberlin.gjots;


import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.KeyStroke;

public class ViewMenu extends JMenu {

	private static final long serialVersionUID = 1L;
	private JComponent[] components ;
	private static final String SETTING_FONT_SIZE="SETTING_FONT_SIZE" ; 

	public ViewMenu() {
		setText("View");
		add(getIncreaseFontAction());
		add(getDecreaseFontAction());
		setVisible(false);
	}


	private void changeFontSize(float amount) {
		float newSize = 0.0f; 
		for (JComponent c : components) {
			Font f = c.getFont();
			newSize = f.getSize2D()+amount;
			c.setFont(f.deriveFont(newSize));
		}
		Version.getPreferences().putFloat(SETTING_FONT_SIZE, newSize);
	}
	
	private void setInitialFontSize() {
		float fontSize = Version.getPreferences().getFloat(SETTING_FONT_SIZE, 0.0f);
		if (fontSize > 0.0f) {
			for (JComponent c : components) {
				Font f = c.getFont();
				c.setFont(f.deriveFont(fontSize));
			}			
		}
	}
	
	
	
	private AppAction getIncreaseFontAction() {
		return new AppAction() {
			@Override
			void init() {
				putValue(Action.NAME, "Increase Font Size");
				putValue(Action.SMALL_ICON, UIUtils.getIcon("/images/baseline_format_size_black_24dp.png"));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, InputEvent.CTRL_MASK));
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_PLUS);

			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				changeFontSize(1f);
			}
		};
	}

	private AppAction getDecreaseFontAction() {
		return new AppAction() {
			@Override
			void init() {
				putValue(Action.NAME, "Decrease Font Size");
				putValue(Action.SMALL_ICON, UIUtils.getIcon("/images/baseline_text_fields_black_24dp.png"));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, InputEvent.CTRL_MASK));
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_MINUS);

			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				changeFontSize(-1f);
			}
		};
	}
	

	public void show(Main main) {
		this.components = new JComponent[] {main.tree, main.edit}; 
		setVisible(true);
		setInitialFontSize();
	}
}
