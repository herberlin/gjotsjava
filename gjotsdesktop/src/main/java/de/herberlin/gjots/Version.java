package de.herberlin.gjots;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Map;
import java.util.Properties;
import java.util.prefs.Preferences;

import de.herberlin.gjots.parser.GjotsParser;

public class Version {

	public static String getVersion() {
		String version = null;
		String file = "/META-INF/maven/de.herberlin.gjots/gjotsparser/pom.properties";
		try {

			Properties props = new Properties();
			props.load(Version.class.getResourceAsStream(file));
			version = props.getProperty("version");
		} catch (Exception e) {
			System.err.println("File not found: " + file);
		}
		if (version == null) {
			version = "development version";
		}
		return version;
	}
	
	public static final boolean isEclipse() {
		Map<String, String> map = System.getenv();
		if (map != null && map.toString().toLowerCase().contains("eclipse"))  {
			return true; 
		} else {
			return false; 
		}
	}

	public static void setSystemErr() {
		try {
			if (!isEclipse() && System.console() == null) {
				System.setErr(new PrintStream(new File("gjotsjava.error")));
			}
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Preferences getPreferences() {
		if (isEclipse()) {
			return Preferences.userNodeForPackage(GjotsParser.class);
		} else {
			return Preferences.userNodeForPackage(Main.class);
		}
	}
}
