package de.herberlin.gjots;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.prefs.Preferences;

/**
 * 
 * WindowPostition
 * 
 * @author (c) Hans Joachim Herbertz
 * @since 16.01.2019
 * @licence Apache 2 http://www.apache.org/licenses/LICENSE-2.0
 */
public class WindowPosition extends ComponentAdapter {

	@Override
	public void componentResized(ComponentEvent e) {
		saveBounds(e.getComponent());
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		saveBounds(e.getComponent());

	}

	private static final String COMPONENT_WIDTH = "COMPONENT_WIDTH";
	private static final String COMPONENT_HEIGHT = "COMPONENT_HEIGHT";
	private static final String COMPONENT_X = "COMPONENT_X";
	private static final String COMPONENT_Y = "COMPONENT_Y";

	public static void saveBounds(Component c) {
		Preferences prefs = Version.getPreferences();
		prefs.putInt(COMPONENT_WIDTH, c.getWidth());
		prefs.putInt(COMPONENT_HEIGHT, c.getHeight());
		prefs.putInt(COMPONENT_X, c.getX());
		prefs.putInt(COMPONENT_Y, c.getY());
	}

	public static void setBounds(Component c) {
		Preferences prefs = Version.getPreferences();
		int x = Math.max(0, prefs.getInt(COMPONENT_X, 0));
		int y = Math.max(0, prefs.getInt(COMPONENT_Y, 0));
		int w = Math.max(320, prefs.getInt(COMPONENT_WIDTH, 400));
		int h = Math.max(240, prefs.getInt(COMPONENT_HEIGHT, 300));
		c.setBounds(x, y, w, h);
		Dimension dimension = new Dimension(w, h);
		c.setPreferredSize(dimension);
		c.setLocation(x, y);
	}
}
