package de.herberlin.gjots;

import java.awt.Point;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

import javax.swing.JTree;
import javax.swing.tree.TreePath;

import de.herberlin.gjots.parser.GjotsParser;
import de.herberlin.gjots.parser.Node;

/**
 * Drop Target for tree. Handles Node move.
 * 
 * @author (c) Hans Joachim Herbertz
 * @since 30.07.2017
 * @licence Apache 2 http://www.apache.org/licenses/LICENSE-2.0
 */
public class DnDSupport extends DropTarget implements DropTargetListener {

	private JTree tree = null;
	private GjotsParser model = null;

	public DnDSupport(JTree tree, GjotsParser model) {
		this.tree = tree;
		this.model = model;
		setComponent(tree);
	}

	@Override
	public void drop(DropTargetDropEvent dtde) {
		TreePath oldPath = tree.getSelectionPath();
		model.moveNode(start, one, two);
		UIUtils.updateTree(tree, oldPath);
		UIUtils.toaster.toast(String.format("Done: %s %s %s%n", start, two == null ? "child of" : "after", one));

		start = null;
		one = null;
		two = null;


	}

	public void dropActionChanged(DropTargetDragEvent dtde) {
		System.out.println(dtde.getDropAction());
	}

	private Node start, one, two;

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		Node node = getTargetNode(dtde.getLocation());
		start = node;
	}

	@Override
	public synchronized void dragExit(DropTargetEvent dte) {
		System.out.println("DragExit " + dte.getSource());
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		Node node = getTargetNode(dtde.getLocation());
		if (node != null) {
			if (start == null) {
				start = node;
			}
			one = node;
		}
		two = node;
		UIUtils.toaster.setStatus(String.format("%s %s %s%n", start, two == null ? "child of" : "after", one));

	}

	private Node getTargetNode(Point point) {
		TreePath tp = tree.getPathForLocation((int) point.getX(), (int) point.getY());
		System.out.printf("%s, %s, %s%n", start, one, tp);
		if (tp != null) {
			tree.setSelectionPath(tp);
			tree.expandPath(tp);
			tree.scrollPathToVisible(tp);
			return (Node) tp.getLastPathComponent();
		}
		return null;
	}
}