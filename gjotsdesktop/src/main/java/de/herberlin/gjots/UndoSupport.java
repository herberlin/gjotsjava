package de.herberlin.gjots;

import java.awt.Component;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

/**
 * As of https://alvinalexander.com/java/java-undo-redo
 */
public class UndoSupport {

	public static void addUndoSupport(JTextComponent textComponent) {
		new UndoSupport(textComponent);

	}

	private Document editorPaneDocument;
	protected UndoHandler undoHandler = new UndoHandler();
	protected UndoManager undoManager = new UndoManager();
	private UndoAction undoAction = null;
	private RedoAction redoAction = null;

	public UndoSupport(JTextComponent jTextPane) {
		this.editorPaneDocument = jTextPane.getDocument();
		removeUndoListeners(editorPaneDocument);
		editorPaneDocument.addUndoableEditListener(new UndoHandler());
		KeyStroke undoKeystroke = KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK);
		KeyStroke redoKeystroke = KeyStroke.getKeyStroke(KeyEvent.VK_H, Event.CTRL_MASK);

		undoAction = new UndoAction();
		undoAction.putValue(Action.ACCELERATOR_KEY, undoKeystroke);
		jTextPane.getInputMap().put(undoKeystroke, "undoKeystroke");
		jTextPane.getActionMap().put("undoKeystroke", undoAction);

		redoAction = new RedoAction();
		redoAction.putValue(Action.ACCELERATOR_KEY, redoKeystroke);
		jTextPane.getInputMap().put(redoKeystroke, "redoKeystroke");
		jTextPane.getActionMap().put("redoKeystroke", redoAction);

		JPopupMenu popup = jTextPane.getComponentPopupMenu();
		if (popup != null) {
			// popup.getActionMap().remove("undoKeystroke");
			// TODO: Remove existing menu entry
			for (Component c : popup.getComponents()) {
				if (c instanceof JMenuItem) {
					JMenuItem item = (JMenuItem)c; 
					if (item.getAction() instanceof UndoSupportAction) {
						popup.remove(c);
					}
				}
			}
			popup.add(undoAction);
			popup.add(redoAction);
		}

	}

	private void removeUndoListeners(Document doc) {
		if (doc instanceof AbstractDocument) {
			UndoableEditListener[] ll = ((AbstractDocument) doc).getUndoableEditListeners();
			if (ll != null) {
				for (UndoableEditListener l : ll) {
					doc.removeUndoableEditListener(l);
				}
			}
		}
	}

	class UndoHandler implements UndoableEditListener {

		@Override
		public void undoableEditHappened(UndoableEditEvent e) {
			undoManager.addEdit(e.getEdit());
			undoAction.update();
			redoAction.update();
		}

	}

	class UndoAction extends AbstractAction implements UndoSupportAction {
		public UndoAction() {
			super("Undo");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			try {
				undoManager.undo();
			} catch (CannotUndoException ex) {
				ex.printStackTrace();
			}
			update();
			redoAction.update();
		}

		protected void update() {
			if (undoManager.canUndo()) {
				setEnabled(true);
				putValue(Action.NAME, "Undo");
			} else {
				setEnabled(false);
				putValue(Action.NAME, "Undo");
			}
		}
	}
	
	interface UndoSupportAction {
		
	}

	class RedoAction extends AbstractAction implements UndoSupportAction {
		public RedoAction() {
			super("Redo");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			try {
				undoManager.redo();
			} catch (CannotRedoException ex) {
				ex.printStackTrace();
			}
			update();
			undoAction.update();
		}

		protected void update() {
			if (undoManager.canRedo()) {
				setEnabled(true);
				putValue(Action.NAME, "Redo");
			} else {
				setEnabled(false);
				putValue(Action.NAME, "Redo");
			}
		}
	}
}
