package de.herberlin.gjots;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.DropMode;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
import javax.swing.text.Highlighter.HighlightPainter;
import javax.swing.tree.TreePath;

import de.herberlin.gjots.UIUtils.Toaster;
import de.herberlin.gjots.crypto.Algo;
import de.herberlin.gjots.parser.Node;

/**
 * User Interface.
 *
 * @author (c) Hans Joachim Herbertz
 * @since 30.07.2017
 * @licence Apache 2 http://www.apache.org/licenses/LICENSE-2.0
 */
public class Main extends JFrame implements Toaster {

	JEditorPane edit = null;
	private DocumentListenerProxy documentListener = new DocumentListenerProxy();
	JTree tree = null;
	private final JLabel status = new JLabel();
	private final Search search = new Search(this);

	public Main() {
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				exit.actionPerformed(null);
			}
		});
		UIUtils.toaster = this;
		try {
			setIconImage(new ImageIcon(getClass().getResource("/logo.png")).getImage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		addComponentListener(new WindowPosition());
		init();
	}

	private GjotsTreeModel model = null;
	private JSplitPane pane = null;
	private static String LAST_FILE = "LAST_FILE";

	@Override
	public void toast(final String text) {
		status.setText(text);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(5000);
				} catch (final InterruptedException e) {
					e.printStackTrace();
				}
				status.setText(getDefaultStatusText());
			}
		}).start();
	}

	private String getDefaultStatusText() {
		String df = "";
		if (model != null && model.getCurrentFile() != null) {
			df = model.getCurrentFile().getAbsolutePath() + (model.isUnsavedChanges() ? "*" : "") + " ("
					+ model.getCurrentAlgo() + ")";
		}
		return df;
	}

	@Override
	public void setStatus(String text) {
		if (text == null) {
			text = getDefaultStatusText();
		}
		status.setText(text);
	}

	public void saveFile(File file, Algo algo) {

		if (model != null) {
			try {
				if (file == null) {
					file = new File(Version.getPreferences().get(LAST_FILE, "default.gjots"));
				} else {
					Version.getPreferences().put(LAST_FILE, file.getAbsolutePath());

				}

				if (algo == null) {
					algo = model.getCurrentAlgo();
				}
				model.save(file, algo, model.getPassword());
				toast("Saved: " + file.getName());

			} catch (final Exception ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(getContentPane(),
						"Error saving to file: " + file + "\n" + ex.getMessage());
			}
		}
	}

	private void makeBackup(File file) {
		try {
			if (!file.getName().endsWith("~")) {
				File backup = new File(file.getAbsolutePath() + "~");
				Files.copy(file.toPath(), backup.toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean loadFile(final File file) {
		final Container c = getContentPane();
		search.setVisible(false);
		viewMenu.setVisible(false);
		if (model != null && model.isUnsavedChanges()) {
			if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this, "Save current file?")) {
				saveFile(null, null);
			}
		}
		model = null;
		try {
			if (!file.exists()) {
				file.createNewFile();
			} else {
				makeBackup(file);
			}
			try {
				// read unencrypted
				model = new GjotsTreeModel(file);
			} catch (final Exception ex) {

				// get password and read encrypted
				final JPanel p = new JPanel();
				p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
				p.add(new JLabel("Please enter password for: " + file.getName() + "."));
				final JPasswordField pf = new JPasswordField();
				p.add(pf);
				new Thread() {
					@Override
					public void run() {
						try {
							Thread.sleep(200);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						pf.requestFocus();
					}
				}.start();
				final int i = JOptionPane.showOptionDialog(getOwner(), p, "Password Dialog",
						JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, "");
				String password = "";
				if (i == JOptionPane.OK_OPTION) {
					password = String.valueOf(pf.getPassword());
				}
				model = new GjotsTreeModel(file, password);

			}

			toast("File loaded: " + file.getName() + ", Encryption: " + model.getCurrentAlgo());

		} catch (final Exception e) {
			JOptionPane.showMessageDialog(c,
					"Error reading file " + file + "\nThis is not a gjots-file or wrong password.");
			return false;
		}

		if (Algo.NONE.equals(model.getCurrentAlgo())) {
			changePassword.setEnabled(false);
		} else {
			changePassword.setEnabled(true);
		}
		Version.getPreferences().put(LAST_FILE, file.getAbsolutePath());
		if (pane != null) {
			c.remove(pane);
		}

		pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		c.add(pane, BorderLayout.CENTER);

		tree = new JTree(model);
		tree.setRootVisible(model.isRootVisible());
		tree.setDragEnabled(true);
		tree.setDropMode(DropMode.ON_OR_INSERT);
		tree.setDropTarget(new DnDSupport(tree, model));
		final JPopupMenu treePop = createPopup(tree, "New Item", "New Child", "Delete");
		tree.setComponentPopupMenu(treePop);
		tree.setEditable(false);
		tree.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(final MouseEvent e) {
				showPop(e);
			}

			@Override
			public void mousePressed(final MouseEvent e) {
				showPop(e);
			}

			private void showPop(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					if (tree.getSelectionPath() != null) {
						tree.getComponentPopupMenu().show(tree, e.getX(), e.getY());
					}
				}
			}

		});
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(final TreeSelectionEvent e) {
				final Node oldNode = UIUtils.getNodeFromTreePath(e.getOldLeadSelectionPath());
				final Node newNode = UIUtils.getNodeFromTreePath(e.getNewLeadSelectionPath());
				if (oldNode != null) {
					edit.getDocument().removeDocumentListener(documentListener);
				}
				if (newNode != null) {
					edit.setText(newNode.getText());
					documentListener.setNode(newNode);
					edit.getDocument().addDocumentListener(documentListener);
					UndoSupport.addUndoSupport(edit);
					edit.setCaretPosition(0);
					String keyword = search.getSearch();
					if (keyword != null && newNode.contains(keyword)) {
						List<Integer> xList = newNode.startPositions(keyword);
						edit.getHighlighter().removeAllHighlights();
						HighlightPainter painter = new DefaultHighlightPainter(Color.YELLOW);
						for (int x : xList) {
							try {
								edit.getHighlighter().addHighlight(x, x + keyword.length(), painter);
								edit.setCaretPosition(x);
							} catch (BadLocationException e1) {
								e1.printStackTrace();
							}
						}
					}
				}

			}
		});
		pane.setLeftComponent(new JScrollPane(tree));

		edit = new JEditorPane();
		final JPopupMenu editPop = createPopup(edit, "copy-to-clipboard", "cut-to-clipboard", "paste-from-clipboard",
				"select-all");
		edit.setComponentPopupMenu(editPop);
		pane.setRightComponent(new JScrollPane(edit));
		viewMenu.show(this);
		WindowPosition.setBounds(this);
		pack();
		pane.setDividerLocation(0.3);

		return true;
	}

	private class PopupActions implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {

			Node node = null;
			if (tree != null) {
				if (tree.getSelectionPath() != null && tree.getSelectionPath().getLastPathComponent() != null) {
					node = (Node) tree.getSelectionPath().getLastPathComponent();

					if (node != null) {
						switch (e.getActionCommand()) {
						case "New Item":
							model.newItemAfter(node);
							break;
						case "New Child":
							model.newChild(node);
							break;
						case "Delete":
							model.deleteNode(node);
							break;
						}
						updateTree();
					}
				} else {
					// append to root
					model.appendToRoot();
					updateTree();

				}

			}
		}
	}

	private void updateTree() {
		final TreePath path = tree.getSelectionPath();
		UIUtils.updateTree(tree, path);
	}

	private final PopupActions popupActions = new PopupActions();

	private JPopupMenu createPopup(final JComponent component, final String... labels) {
		final JPopupMenu pop = new JPopupMenu();
		if (component != null) {
			for (final String s : labels) {
				final JMenuItem item = new JMenuItem();
				final Action a = component.getActionMap().get(s);
				if (a != null) {
					item.setAction(a);
					item.setText(UIUtils.ucFirst(item.getText()));
				} else {
					item.setText(s);
					item.addActionListener(popupActions);
				}
				pop.add(item);
			}
		}
		return pop;
	}

	private Action openFile, saveFile, saveAs, changePassword, exit, newGjots;
	private ViewMenu viewMenu = new ViewMenu();

	// https://stackoverflow.com/questions/9370326/default-action-button-icons-in-java
	private void createGlobalActions() {
		openFile = new AppAction() {
			@Override
			void init() {
				putValue(Action.NAME, "Open File");
				putValue(Action.SMALL_ICON, UIUtils.getIcon("/images/ic_open_in_browser_black_24dp_1x.png"));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);

			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				JFileChooser chooser = null;
				final String filename = Version.getPreferences().get(LAST_FILE, null);
				if (filename != null) {
					chooser = new JFileChooser(new File(filename));
				} else {
					chooser = new JFileChooser();
				}
				final int val = chooser.showOpenDialog(getContentPane());
				if (val == JFileChooser.APPROVE_OPTION) {
					final File file = chooser.getSelectedFile();
					loadFile(file);
				}
			}
		};

		saveFile = new AppAction() {
			@Override
			void init() {
				putValue(Action.NAME, "Save");
				putValue(Action.SMALL_ICON, UIUtils.getIcon("/images/ic_system_update_alt_black_24dp_1x.png"));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);

			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				saveFile(null, null);
			}
		};
		saveAs = new AppAction() {
			@Override
			void init() {
				putValue(Action.NAME, "Save as..");
				putValue(Action.SMALL_ICON, UIUtils.getIcon("/images/ic_vertical_align_bottom_black_24dp_1x.png"));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_W);
			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				Dialogs.saveAsDialog(Main.this);
			}
		};
		changePassword = new AppAction() {
			@Override
			void init() {
				putValue(Action.NAME, "Change Password");
				putValue(Action.SMALL_ICON, UIUtils.getIcon("/images/ic_settings_black_24dp_1x.png"));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_P);
			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				Dialogs.changePasswordDialog(Main.this, null, null);
			}
		};

		exit = new AppAction() {
			@Override
			void init() {
				putValue(Action.NAME, "Exit");
				putValue(Action.SMALL_ICON, UIUtils.getIcon("/images/ic_exit_to_app_black_24dp_1x.png"));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);

			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				if (model != null && model.isUnsavedChanges()) {
					final int result = JOptionPane.showConfirmDialog(getOwner(), "Save changes?");
					if (JOptionPane.YES_OPTION == result) {
						saveFile(null, null);
						System.exit(0);
					} else if (JOptionPane.NO_OPTION == result) {
						System.exit(0);
					} else {

					}
				} else {
					System.exit(0);
				}

			}
		};

		newGjots = new AppAction() {
			@Override
			void init() {
				putValue(Action.NAME, "New Gjots");
				putValue(Action.SMALL_ICON, UIUtils.getIcon("/images/ic_note_add_black_24dp_1x.png"));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
				putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);

			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				loadFile(new File("new.gjotsjava"));
			}
		};

		final JMenuBar bar = new JMenuBar();
		final JMenu menu = new JMenu("File");
		menu.add(new JMenuItem(newGjots));
		menu.add(new JMenuItem(openFile));
		menu.add(new JMenuItem(saveFile));
		menu.add(new JMenuItem(saveAs));
		menu.add(new JMenuItem(changePassword));
		menu.add(new JMenuItem(exit));
		bar.add(menu);
		bar.add(search.getSearchMenu());
		bar.add(viewMenu);
		setJMenuBar(bar);
	}

	private void init() {
		setTitle("GjotsJava " + Version.getVersion());
		final Container c = getContentPane();
		c.setLayout(new BorderLayout());
		createGlobalActions();
		final int padding = 5;
		status.setBorder(new EmptyBorder(padding, padding, padding, padding));
		c.add(status, BorderLayout.SOUTH);

		final String fileName = Version.getPreferences().get(LAST_FILE, null);
		add(search, BorderLayout.NORTH);
		WindowPosition.setBounds(this);
		invalidate();
		

		if (fileName != null) {
			final File file = new File(fileName);
			if (file.exists()) {
				if (!loadFile(file)) {
					this.dispose();
					System.exit(1);
				}
			}
		} 
	}

	public GjotsTreeModel getModel() {
		return model;
	}

	public JTree getTree() {
		return tree;
	}

	public static void main(final String[] args) {

		Version.setSystemErr();

		System.out.println("GjotsJava " + Version.getVersion());
		System.out.println("https://bitbucket.org/herberlin/gjotsjava");
		new Main().setVisible(true);
	}
}
